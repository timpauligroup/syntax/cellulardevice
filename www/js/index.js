"use strict";

let app = {
  initialize: function() {
    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
  },
  onDeviceReady: function() {
    console.log('deviceready');
    document.addEventListener("pause", this.onPause.bind(this), false);
    document.addEventListener("resume", this.onResume.bind(this), false);
    start_audio();
  },
  onPause: function() {
    // Handle the pause event
  },

  onResume: function() {
    // Handle the resume event
  }
};

app.initialize();
